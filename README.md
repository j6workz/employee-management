# This is a Sample Project

This single page app project with a scenario of Employee Management is intended to sample my skills over ReactJS and other related libraries and packages.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

