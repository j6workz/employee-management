import { Fragment } from "react";
import { Container } from "react-smooth-dnd";
import { makeStyles, colors } from "@material-ui/core";
import DraggableCard from "../DraggableCard";
import _ from "lodash";
import classnames from 'classnames';

const useStyles = makeStyles((theme) => ({
  containerWrapper: {
    display: "flex",
  },
  verticalWrapper: {
    position: 'relative',
  },
  horizontalWrapper: {
    '&$intermediateWrapper:before':{
      content: '" "', position: 'absolute', height: '100%', width: 4, background: 'black', left: `-73px`, top: 0
    }
  },
  intermediateWrapper: {
    position: 'relative',
  },
  isEmptyContainerWrapper: {
    width: 150, height: 150, background: `${colors.yellow[500]}25`,
    '& .smooth-dnd-container': {
      minWidth: '100%', minHeight: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center',
      '&:after': {
        content: '" Drop a SubOrdinate to Add "', fontSize: '16px', color: 'grey'
      }
    }
  },
  topContainer: {
  },
  root: {
    minWidth: 275,
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
}));


const recursiveRenderer = (data, classes, isStart=false, isWholeLast, updateReportingTo) => {
  return _.map(data, (o, i) => {
    let isHorizontal = o?.level % 2 === 0 ? true : false;
    let isLast = data.length-1 === i;
    return (<Fragment  key={`container_wrapper_${
      isHorizontal ? "horizontal" : "vertical"
    }_level_${o?.level}_${i}`}>
      {
        _.isEmpty(o?.subOrds) && o?.level === 2
          ? <>
              <DraggableCard  name={o?.name} role={o?.designation} image={o?.profile} isStart={isStart} isMiddle={!isLast} isLast={isLast} isHorizontal={!isHorizontal} hasSubOrds={!_.isEmpty(o?.subOrds)} />
              <div
                key={`container_wrapper_${isHorizontal ? "horizontal" : "vertical"}_level_${o?.level}_${i}`}
                style={isHorizontal || isStart ? { marginLeft: 73 } : { marginTop: 300 }}
                className={classnames(classes.containerWrapper, { [classes.horizontalWrapper]: isHorizontal, [classes.verticalWrapper]: !isHorizontal, [classes.intermediateWrapper]: isWholeLast && !isLast, [classes.isEmptyContainerWrapper]: true })}
              >
                <Container
                  orientation={isHorizontal ? "horizontal" : "vertical"}
                  groupName={`employees_level_${o?.level}`} 
                  getChildPayload={i =>({ i, o })}
                  onDrop={e => {
                    if (_.isEmpty(e?.removedIndex) && _.isNumber(e?.addedIndex)) {
                      updateReportingTo(e?.payload?.o?.subOrds[e?.payload?.i]?.id, o?.id, o?.level);
                    }
                  }}
                >
                </Container>
              </div>
            </> 
          : <DraggableCard name={o?.name} role={o?.designation} image={o?.profile} isStart={isStart} isLast={isLast} isHorizontal={!isHorizontal} hasSubOrds={!_.isEmpty(o?.subOrds)} />
      }

      {
        _.isEmpty(o?.subOrds) ? null : (
        <div
         
          style={isHorizontal || isStart ? {marginLeft: 73} : {marginTop: 300}}
          className={classnames(classes.containerWrapper, {[classes.horizontalWrapper]: isHorizontal, [classes.verticalWrapper]: !isHorizontal, [classes.intermediateWrapper]: isWholeLast && !isLast})}
        >
          <Container
            orientation={isHorizontal ? "horizontal" : "vertical"}
            groupName={`employees_level_${o?.level}`} 
            getChildPayload={i => ({i, o})}
              onDrop={e => {
                console.log('on drop ------- ', e, o);
                if(_.isEmpty(e?.removedIndex) && _.isNumber(e?.addedIndex)){
                  updateReportingTo( e?.payload?.o?.subOrds[e?.payload?.i]?.id, o?.id, o?.level);
                }
              }}
          >
            {recursiveRenderer(o?.subOrds, classes, false, isLast, updateReportingTo)}
          </Container>
        </div>
        )
      }
    </Fragment>);
  });
};



const TreeView = (props) => {
  const classes = useStyles();
  const {data = {}, handleReportingToChange = ()=>null} = props;

  return <Container>{recursiveRenderer(data, classes, true, false, handleReportingToChange)}</Container>;
};

export default TreeView;