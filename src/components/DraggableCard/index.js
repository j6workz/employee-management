import { Card, makeStyles, CardContent, Typography, CardMedia, colors } from "@material-ui/core";
import { Draggable } from "react-smooth-dnd";
import classnames from 'classnames';

const useStyles = makeStyles((theme) => ({
  cardContainer: {
    width: 150, height: 150, display: 'flex'
  },
  inArrow: props => {
    if(props?.isStart){
      return null
    }
    if(props?.isHorizontal){
      return({
        height: '100%', width: 4, background: 'black'
      })
    }else{
      return({
      })
    }
  },
  leftSentinel: {
    width: 25, height: '100%', boxSizing: 'border-box',
    display: 'flex', alignItems: 'center', position: 'relative',
    '&:before': {
      content: '" "', position: 'absolute', background: 'black', top: 0, left: 0
    },
    '& $inArrow': {
      width: '100%', height: 4, background: 'black'
    }
  },
  sentinelArrowLineWidth: props => {
    if(props?.isStart){
      return null
    }
    if(props?.isHorizontal){
      return({
        '&:before': {
          height: 4, width: '100%'
        }
      })
    }else{
      return({
        '&:before':{
          height: props?.isLast ? '50%' : '100%', width: 4
        }
      })
    }
  },
  rightSentinel: props => ({
    width: 25, height: '100%', boxSizing: 'border-box',
    display: 'flex', alignItems: 'center', position: 'relative',
    '&:before': {
      content: '" "', position: 'absolute', background: 'black', top: 0, left: 0
    },
    '&$sentinelArrowLineWidth': !props?.hasSubOrds ? {} : {'&:before': {width: 0, height: 0}}
  }),
  centerPortion: {
    display: 'flex', flexDirection: 'column', flex: 1
  },
  topSentinel: {
    height: 25, width: '100%', boxSizing: 'border-box', position: 'relative', display: 'flex', justifyContent: 'center',
  },
  topSentinelInLine: props => {
    if(props?.isStart){
      return null
    }else{
      return({
        '&:before': {
          content: '" "', position: 'absolute', top: 0, left: 0,
          height: 4, width: props?.isLast ? '50%' : '100%', background: 'black'
    
        }
      })
    }
  },
  bottomSentinel: {
    height: 25, width: '100%', boxSizing: 'border-box',
    display: 'flex', justifyContent: 'center',
    '& $outArrow': {
      width: 4, height: '100%', background: 'black'
    }
  },
  outArrow: {},
  cardPortion: {
    width: '100%', height: '100%',
    '& .MuiCardContent-root': {
      padding: 0, paddingBottom: 0
    },
    '& .MuiTypography-h5': {
      fontSize: theme.typography.pxToRem(14),
      lineHeight: theme.typography.pxToRem(18)
    }
  },
  root: {
    width: '100%',
    height: '100%',
    border: `1px solid #ececec`,
    boxSizing: 'border-box',
    position: "relative",
    overflow: "visible",
    display: 'flex', gap: `8px`, flexDirection: 'column', padding: theme.spacing(1)
  },
  cardMedia: {
    width: '100%',
    height: '50%',
    backgroundSize: 'contain'
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    fontSize: 12, lineHeight: `14px`, color: colors.grey[500]
  },
}));

const DraggableCard = ({ name = "Qwerty", image = '', role = '', isStart = false, isMiddle=false, isLast=false, isHorizontal=false, hasSubOrds=false }) => {
  const classes = useStyles({isLast, isHorizontal, isStart, hasSubOrds});

  return (
    <Draggable>
      <div className={classes.cardContainer}>

        <div className={classnames(classes.leftSentinel, classes.sentinelArrowLineWidth)}>
          {!isHorizontal && <div className={classes.inArrow} />}
        </div>

        <div className={classes.centerPortion}>
          <div className={classnames(classes.topSentinel, {[classes.topSentinelInLine]: isHorizontal})}>
            <div className={classes.inArrow} />
          </div>

          <div className={classes.cardPortion}>
            <Card className={classes.root}>
              <CardMedia className={classes.cardMedia} image={image} title={name} />
              <CardContent>
                <Typography variant="h5" component="h2">
                  {name}
                </Typography>
                <Typography className={classes.pos} color="textSecondary">
                  {role}
                </Typography>
              </CardContent>
            </Card>
          </div>

          <div className={classes.bottomSentinel}>
          {(!isHorizontal || isStart) && hasSubOrds && <div className={classes.outArrow} />}
          </div>
        </div>

        <div className={classnames(classes.rightSentinel, {[classes.sentinelArrowLineWidth]: !isLast && !(isMiddle && !hasSubOrds)})}>
        </div>

      </div>
    </Draggable>
  );
};

export default DraggableCard;
