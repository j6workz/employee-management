import _ from "lodash";
import update from 'immutability-helper';

let searchEmpTimeout;

const mockAPIs = {
    getAllEmployees: () => {
        return new Promise((resolve, reject)=>{
            setTimeout(()=>{
                resolve(data);
            }, 500)
        })
    },
    searchEmployee: (keyword) => {
        if(!!searchEmpTimeout){
            clearTimeout(searchEmpTimeout);
        }
        return new Promise((resolve, reject) => {
            searchEmpTimeout = setTimeout(()=>{
                let temp = _.filter(data, o=> new RegExp(`\\b(\\w*${_.escapeRegExp(keyword)}\\w*)\\b`, "gi").test(`${o?.name} ${o?.designation} ${o?.team} ${_.find(data, j=>j?.id === o?.managerID)?.name}`) );
                resolve(temp)
            },150)
        })
    },
    updateManager: (toChangeIdx, newManagerId, parentLevel, currentStateData) => {
        return new Promise((resolve, reject) => {
            setTimeout(()=>{
                resolve(updateReportingTo(currentStateData, toChangeIdx, newManagerId, parentLevel))
            },0)
        })
    }
}

const updateReportingTo = (data, idOfChangedEmp, newReportingTo, parentLevel) => {
    let idx = _.findIndex(data, o => o?.id === idOfChangedEmp);
    let temp = update(data, { [idx]: { $merge: { managerID: newReportingTo, level: parentLevel + 1 } } });
    return temp;
}

export default mockAPIs;

const data = [
    {id: 'emp_1', name: 'Mark Hill', designation: 'CEO', level: 1, profile: 'https://randomuser.me/api/portraits/lego/1.jpg', managerID: null, team: 'Management Team'},
    {id: 'emp_2', name: 'Joe Linux', designation: 'CTO', level: 2, profile: 'https://randomuser.me/api/portraits/lego/2.jpg', managerID: 'emp_1', team: 'Technical Team'},
    {id: 'emp_3', name: 'Linda Mary', designation: 'CBO', level: 2, profile: 'https://randomuser.me/api/portraits/lego/3.jpg', managerID: 'emp_1', team: 'Business Team'},
    {id: 'emp_4', name: 'John Green', designation: 'CAO', level: 2, profile: 'https://randomuser.me/api/portraits/lego/4.jpg', managerID: 'emp_1', team: 'Accounts Team'},
    {id: 'emp_5', name: 'Ron Blonquist', designation: 'CISO', level: 3, profile: 'https://randomuser.me/api/portraits/lego/5.jpg', managerID: 'emp_2', team: 'Technical Team'},
    {id: 'emp_6', name: 'Michael Rubin', designation: 'CIO', level: 3, profile: 'https://randomuser.me/api/portraits/lego/6.jpg', managerID: 'emp_2', team: 'Technical Team'},
    {id: 'emp_7', name: 'Alice Lopez', designation: 'CCO', level: 3, profile: 'https://randomuser.me/api/portraits/lego/9.jpg', managerID: 'emp_3', team: 'Business Team'},
    {id: 'emp_8', name: 'Mary Johnson', designation: 'CBO', level: 3, profile: 'https://randomuser.me/api/portraits/lego/8.jpg', managerID: 'emp_3', team: 'Business Team'},
    {id: 'emp_9', name: 'Kirk Douglas', designation: 'CBDO', level: 3, profile: 'https://randomuser.me/api/portraits/lego/7.jpg', managerID: 'emp_3', team: 'Business Team'},
    {id: 'emp_10', name: 'Erica Reel', designation: 'CCusO', level: 3, profile: 'https://randomuser.me/api/portraits/lego/9.jpg', managerID: 'emp_4', team: 'Accounts Team'},
  ];