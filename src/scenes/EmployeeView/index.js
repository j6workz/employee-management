import { useEffect, useState } from 'react';
import _ from 'lodash';
import {Avatar, CircularProgress, colors, Icon, List, ListItem, ListItemAvatar, ListItemText, makeStyles, TextField, Typography} from '@material-ui/core';
import TreeView from "../../components/TreeView";
import mockAPIs from '../../services/mockAPIs';
import { arrayToObjNester } from '../../utils';


const useStyles = makeStyles(theme=>({
    root: {
        width: '100%', maxHeight: '100vh', height: '100vh', boxSizing: 'border-box',
        display: 'flex', flex: 1, flexDirection: 'column'
    },
    header: {
        width: '100%', height: 48, 
        boxSizing: 'border-box', flexShrink: 0, display: 'flex', justifyContent: 'center', alignItems: 'center',
        fontSize: 24, fontWeight: 500, letterSpacing: 1,
        boxShadow: `0px 0px 4px 0px ${colors.grey[400]}`, zIndex: 1
    },
    mainSection: {
        display: 'flex', flex: 1, maxHeight: `calc(100% - 48px)`
    },
    leftPane: {
        width: 300, height: '100%', maxHeight: '100%', 
        padding: theme.spacing(2),
        boxSizing: 'border-box',
        boxShadow: `1px 4px 2px 0px ${colors.grey[300]}`
    },
    optionsContainer: {
        width: '100%', flex: 1, 
        boxSizing: 'border-box', 
        maxHeight: `calc(100% - 24px)`, overflow: 'auto'
    },
    listItemRoot: {
        marginTop: theme.spacing(1), marginBottom: theme.spacing(1), borderRadius: 4, background: colors.grey[100],
        '&:hover': {
            cursor: 'pointer',
            background: colors.grey[200],
            boxShadow: `0px 0px 3px 0px ${colors.grey[300]}`
        },
        '& .MuiListItemText-multiline': {
            marginTop: theme.spacing(0), marginBottom: theme.spacing(0),
        }
    },
    listItemPrimaryText: {
        fontSize: theme.typography.pxToRem(16), fontWeight: 400, color: colors.grey[800]
    },
    listItemSecondaryText: {
        fontSize: theme.typography.pxToRem(13), fontWeight: 400, color: colors.grey[500]
    },
    treeView: {
        boxSizing: 'border-box', height: '100%',
        display: 'flex', flex: 1, 
        overflow: 'auto', justifyContent: 'center',
    }
}));

const EmployeeView = () => {

    const classes = useStyles();
    const [data, setData] = useState([]);
    const [nestedData, setNestedData] = useState({});
    const [dataFetchStatus, setDataFetchStatus] = useState({isLoading: true, keyword: '', isError: false, errMsg: ''});

    useEffect(() => {
        setNestedData(arrayToObjNester(data))
    }, [data]);

    useEffect(()=>{
        setDataFetchStatus({ ...dataFetchStatus, isLoading: true });
        mockAPIs[!!dataFetchStatus?.keyword ? 'searchEmployee' : 'getAllEmployees'](dataFetchStatus?.keyword).then(resp => {
            setData(resp);
            setDataFetchStatus({ ...dataFetchStatus, isLoading: false });
        }).catch(err => {
            console.log('error in api', err);
            setDataFetchStatus({ ...dataFetchStatus, isLoading: false, isError: true, errMsg: err });
        })
    }, [dataFetchStatus?.keyword]);

    const handleKeywordChange = _.debounce((v)=>setDataFetchStatus({...dataFetchStatus, keyword: v}), 200)

    const handleReportingToChange = (toChangeIdx, newReportingToId, parentLevel) => {
        mockAPIs.updateManager(toChangeIdx, newReportingToId, parentLevel, data).then(resp => {
            setData(resp)
        })
    }

    return (
        <div className={classes.root}>
            <div className={classes.header}>
                Employee Management
            </div>
            <div className={classes.mainSection}>
                <div className={classes.leftPane}>
                    <TextField fullWidth defaultValue={dataFetchStatus?.keyword || ''} 
                    onChange={e => handleKeywordChange(e.target.value)}
                    placeholder='Search Employees' variant='outlined' margin='none' size='small' InputProps={{endAdornment: dataFetchStatus?.isLoading ? <CircularProgress size={16} /> : <Icon>search</Icon>}} />
                    <List className={classes.optionsContainer}>
                        {
                            _.map(data, (o,i)=><EmployeeListItem key={`employee_list_item_${i}`} name={o?.name} role={o?.designation} team={o?.team} classes={classes} profile={o?.profile} />)
                        }
                    </List>
                </div>
                <div className={classes.treeView}>
                    {
                        dataFetchStatus?.isLoading
                            ?   <CircularProgress style={{alignSelf: 'center'}} size={100} />
                            :   <TreeView data={nestedData} handleReportingToChange={handleReportingToChange} />
                    }
                </div>
            </div>
        </div>
    )
}

const EmployeeListItem = ({name, role, team, profile, classes}) => {
    return (<>
        <ListItem alignItems="flex-start" className={classes.listItemRoot}>
            <ListItemAvatar>
                <Avatar alt={name} src={profile} />
            </ListItemAvatar>
            <ListItemText
                primary={<Typography variant='body1' className={classes.listItemPrimaryText}>{name}</Typography>}
                secondary={<Typography component="span" variant="body2" className={classes.listItemSecondaryText} color="textPrimary">{role} - {team}</Typography>}
            />
        </ListItem>
    </>)
}

export default EmployeeView;