import _ from 'lodash';

export const arrayToObjNester = (data) => {
    if (_.isArray(data)) {
        let levelWise = _.reduce(
            data,
            (acc, o, i) => {
                if (_.isEmpty(acc[`level_${o?.level}`])) {
                    acc[`level_${o?.level}`] = [o];
                } else {
                    acc[`level_${o?.level}`].push(o);
                }
                // console.log("acc ------------", acc);
                return acc;
            },
            {}
        );
        let hierarchialWise = _.reduce(
            levelWise,
            (acc, val, key) => {
                let t = _.map(val, (o, i) => {
                    let subOrds = _.filter(
                        levelWise[`level_${_.toLength(key.match(/\d+/)[0]) + 1}`],
                        (s, sIdx, sColl) => {
                            // console.log(
                            //   "filtered value as s ----------- ",
                            //   s,
                            //   s?.reportingTo,
                            //   o?.id,
                            //   sColl
                            // );
                            return s?.managerID === o?.id;
                        }
                    );
                    o.subOrds = subOrds?.length > 0 ? subOrds : null;
                    return o;
                });
                acc[key] = t;
                return acc;
            },
            {}
        );
        // console.log(
        //   "hierarchial wise --------- ",
        //   levelWise,
        //   hierarchialWise["level_1"]
        // );
        let keys = Object.keys(hierarchialWise);
        return hierarchialWise[keys[0]];
    } else {
        return data;
    }
};