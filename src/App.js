import './App.css';
import EmployeeView from './scenes/EmployeeView';

function App() {
  return (
    <div className="App">
      <EmployeeView />
    </div>
  );
}

export default App;
